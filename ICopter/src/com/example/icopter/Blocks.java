package com.example.icopter;

import java.util.ArrayList;
import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.util.Log;

public class Blocks {
	private Random generator = new Random();
	public static ArrayList<Rect> blocks;
	public int blockWidth;
	public int blocksCount;
	private int[] blockHeights={100,120,140,160,180,200,220,240};
	private int[] obstacleMovements={-200,-150,-100,-50,0,50,100,150,200};
	public static Rect obstacle1;
	public static Rect obstacle2;
	public Bitmap obstacleBitmap;
	public int blocks_speed=10;
	public static boolean moveBlockHeights = true;
	Paint p = new Paint();
	public int score_addition = 1;

	public Blocks(){
		blocks = new ArrayList<Rect>();
		initiateBlockSizes();
	}
	
	public void addBlock(Rect block){
		blocks.add(block);
	}
	
	public ArrayList<Rect> getBlocks(){
		return blocks;
	}
	
	public void moveObstacles(){
		if(obstacle1.left<=0){
//			If the first obstacle arrives at the start, check randomly whether to create bonus or not
			if(!Variables.bonus.movingTrooper)
				Variables.bonus.movingTrooper=Variables.bonus.createBonus(50);
			if(!Variables.bonus.jetForm){
				Variables.bonus.movingEagle=Variables.bonus.createBonus(80);
			}
			int moveBlock = obstacleMovements[generator.nextInt(8)];
			obstacle1.left = Variables.screenWidth-blockWidth;
			obstacle1.right = Variables.screenWidth;
			//Check that obstacle movement won't go above/below screen
			if(obstacle1.top+moveBlock>=50 && obstacle1.bottom+moveBlock<=Variables.screenHeight-50){
				obstacle1.top+=moveBlock;
				obstacle1.bottom+=moveBlock;
			}
			else{
				obstacle1.top+=-moveBlock;
				obstacle1.bottom+=-moveBlock;
			}
		}
		else if(obstacle2.left<=0){
			int moveBlock = obstacleMovements[generator.nextInt(8)];
			obstacle2.left = Variables.screenWidth-blockWidth;
			obstacle2.right = Variables.screenWidth;
			//Check that obstacle movement won't go above/below screen
			if(obstacle2.top+moveBlock>=50 && obstacle2.bottom+moveBlock<=Variables.screenHeight-50){
				obstacle2.top+=moveBlock;
				obstacle2.bottom+=moveBlock;
			}
			else{
				obstacle2.top+=-moveBlock;
				obstacle2.bottom+=-moveBlock;
			}
		}
		obstacle1.left+=-blocks_speed;
		obstacle1.right+=-blocks_speed;
		obstacle2.left+=-blocks_speed;
		obstacle2.right+=-blocks_speed;
	}
	
	public void moveBlocks(){
		int randomHeightMovement = blockHeights[1];
		for(Rect r:blocks){
			if(r.left<=-50){
				r.left=Variables.screenWidth-blockWidth+25;
				r.right=Variables.screenWidth+25;
				//Detect whether block is a top (0, y) or a bottom one
				//Randomly change the height of the block
				if(r.top==0){
					if (moveBlockHeights){randomHeightMovement = blockHeights[generator.nextInt(6)];}
					r.bottom=randomHeightMovement;
				}
				else{
					if (moveBlockHeights){randomHeightMovement = blockHeights[generator.nextInt(6)];}
					r.top=Variables.screenHeight-100-(randomHeightMovement);
				}
			}
			r.left+=-blocks_speed;
			r.right+=-blocks_speed;
		}
		Variables.score+=score_addition;
	}
	public int getBlockWidth(){
		return blockWidth;
	}
	
    
    public void initiateBlockSizes(){
    	//Calculate the blocks count and width according to screen size
    	obstacleBitmap = 
    			BitmapFactory.decodeResource(Variables.mContext.getResources(), R.drawable.block);
    	for(int i=20;i<120;i++){
    		if(Variables.screenWidth % i==0){
    			blocksCount = i;
    			blockWidth = Variables.screenWidth/i;
    			break;
    		}
    	}
    	
    	int left = Variables.screenWidth-blockWidth+25;
    	int right = Variables.screenWidth+25;
    	//Create two obstacle blocks
    	obstacle1 = new Rect(left, (Variables.screenHeight/2)-100, right, (Variables.screenHeight/2)+100);
    	obstacle2 = new Rect(left+right/2, (Variables.screenHeight/2)-100, right+right/2, (Variables.screenHeight/2)+100);
    	
    	
    	//Add blocks to the list according to blocksCount
		for(int i=0;i<blocksCount;i++){
			addBlock(new Rect(left, 0, right, 100));
			addBlock(new Rect(left, Variables.screenHeight-200, right, Variables.screenHeight));
			left = left - getBlockWidth();
			right = right - getBlockWidth();
		}
		
		Log.i("TAG",obstacle1.toString());
    }
    
    public boolean detectCollison(Rect copter){
    	for(Rect r:blocks){
    		if(Rect.intersects(r, copter)){
    			return true;
    		}
    	}
    	if(Rect.intersects(obstacle1, copter) || Rect.intersects(obstacle2, copter)){
    		return true;
    	}
    	return false;
    	
    }
    
	public void drawBlocks(Canvas canvas){
		p.setColor(Color.BLACK);
		p.setStyle(Paint.Style.FILL);
//		Log.i("TAG",""+blocks.size());
		canvas.drawBitmap(obstacleBitmap, null, obstacle1, null);
		canvas.drawBitmap(obstacleBitmap, null, obstacle2, null);
		for(int i=0;i<blocks.size();i++){
			p.setColor(Color.parseColor("#C98A2C"));
			p.setStyle(Paint.Style.FILL);
			canvas.drawRect(blocks.get(i), p);
			p.setStyle(Paint.Style.STROKE);
//			p.setColor(Color.BLACK);
			p.setStrokeWidth(2);
			canvas.drawRect(blocks.get(i), p);
			canvas.drawBitmap(obstacleBitmap, null, blocks.get(i), null);
		}
	
//		Log.i("TAG",obstacle1.toString());
//		Log.i("TAG2 ",blocks.get(1).toString());
		moveObstacles();
		moveBlocks();
	}
	

}
