package com.example.icopter;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.DisplayMetrics;

public class Variables extends Application {
	public static Context mContext;
	public static DisplayMetrics metrics; 
	public static int screenWidth;
	public static int screenHeight;
	public static Blocks blocks;
	public static Copter copter;
	public static BonusObjects bonus;
	public static long score = 0;
	public static SharedPreferences mPrefs;
	public static SharedPreferences.Editor editor;
	public static int highscore;



	@Override
	public void onCreate() {
		super.onCreate();
		mContext = this.getApplicationContext();
		metrics = mContext.getResources().getDisplayMetrics();    
		screenWidth = metrics.widthPixels;
		screenHeight = metrics.heightPixels;
		blocks = new Blocks();
		copter = new Copter(mContext);
		bonus = new BonusObjects();
		mPrefs = mContext.getSharedPreferences("ICopter", Context.MODE_PRIVATE);
		editor= mPrefs.edit();
		highscore = mPrefs.getInt("Highscore", 0);
				
	}

	public static Context getContext(){
		return mContext;
	}

	public static Bitmap RotateBitmap(Bitmap source, float angle)
	{
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}



	public static Bitmap makeTransparent(Bitmap src, int value) {  
		int width = src.getWidth();
		int height = src.getHeight();
		Bitmap transBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(transBitmap);
		canvas.drawARGB(0, 0, 0, 0);
		final Paint paint = new Paint();
		paint.setAlpha(value);
		canvas.drawBitmap(src, 0, 0, paint);    
		return transBitmap;
	}

	public static void changeBitmapColor(int color,Bitmap mp){
		Paint pnt = new Paint();
		Canvas myCanvas = new Canvas(mp);

		int myColor = mp.getPixel(0, 0);

		// Set the colour to replace.
		ColorFilter filter = new LightingColorFilter(myColor, color);
		pnt.setColorFilter(filter);

		// Draw onto new bitmap. result Bitmap is newBit
		myCanvas.drawBitmap(mp,0,0, pnt);
	}


}
