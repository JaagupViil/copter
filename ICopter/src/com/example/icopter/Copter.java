package com.example.icopter;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

public class Copter {
	Bitmap copter;
	Bitmap smoke;
	public int copterY;
	public int copterX;
	static Rect upperCopter;
	static Rect lowerCopter;
	static ArrayList<Rect> smokes;
	public boolean touchScreen;
	public int copter_speed=8;
	public static int smoke_speed=Variables.blocks.blocks_speed-5;
	Paint p = new Paint();

	public Copter(Context context){
		smokes = new ArrayList<Rect>();
		copter = BitmapFactory.decodeResource(context.getResources(), R.drawable.copter);
		smoke = BitmapFactory.decodeResource(context.getResources(), R.drawable.smoke);
		smoke = Variables.makeTransparent(smoke, 70);
//		Variables.changeBitmapColor(Color.GREEN, smoke);
		initiateCopter();
	}
	public void setCopterToBitmap(){
		this.copter= BitmapFactory.decodeResource(Variables.mContext.getResources(), R.drawable.copter);
	}
	public void setBitmap(Bitmap m){
		this.copter=m;
	}
	public int getCopterY() {
		return copterY;
	}

	public void setCopterY(int copterY) {
		this.copterY = copterY;
	}

	public void updateCoordinates(){
		upperCopter.set(copterX+5,copterY+0,copterX+150,copterY+40);
		lowerCopter.set(copterX+80,copterY+50,copterX+150,copterY+70);
	
	}
	//Cover copter picture with actual rectangles => collision detection easier
	public void initiateCopter(){
		int centerY = Variables.screenHeight/2;
		copterY = centerY;
		copterX = centerY;
		upperCopter = new Rect();
		lowerCopter = new Rect();
		for(int i=0;i<19;i++){
//			//jet kind of smoke, note: flickering
//			smokes.add(new Rect(copterX-i*35,copterY+0,copterX+10,copterY+40));
			smokes.add(new Rect(copterX+15-i*25,copterY+15,copterX+80-i*25,copterY+80));
		}
//		smokes.add(new Rect(copterX,copterY,copterX+10,copterY+40));
	}
	
	public void moveSmokeForward(){
		for(Rect r:smokes){
			if(r.left<=100){
				r.left=copterX+15;
				r.right=copterX+80;
				r.top=copterY+15;
				r.bottom=copterY+80;
			}
			r.left+=-smoke_speed;
			r.right+=-smoke_speed;
		}
	}
	
	
	public void moveCopterDown(){
		copterY = copterY+copter_speed;
		moveSmokeForward();
		updateCoordinates();

	}
	public void moveCopterUp(){
		copterY = copterY-copter_speed;
		moveSmokeForward();
		updateCoordinates();
	}
	public void drawSmoke(Canvas canvas){
		for(int i=0;i<smokes.size();i++){
			canvas.drawBitmap(smoke,null,smokes.get(i),null);
		}

	}
	public void drawCopter(Canvas canvas){
		p.setColor(Color.CYAN);
//		canvas.drawRect(lowerCopter, p);
//		canvas.drawRect(upperCopter, p);
		drawSmoke(canvas);
		Bitmap resizedbitmap = Bitmap.createScaledBitmap(copter, 150, 70, true);
		if(touchScreen){
			moveCopterUp();
			canvas.drawBitmap(Variables.RotateBitmap(resizedbitmap,5), copterX, copterY, null);
		}
		else{
			canvas.drawBitmap(resizedbitmap, copterX, copterY, null);
			moveCopterDown();
		}

	}

}
