package com.example.icopter;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity implements OnCompletionListener{
	
	MediaPlayer player;
	BackgroundSound mBackgroundSound;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		//Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		mBackgroundSound = new BackgroundSound();
		setContentView(R.layout.background);
	
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mBackgroundSound.cancel(true);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mBackgroundSound.doInBackground(null);
	}



	public class BackgroundSound extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			player = MediaPlayer.create(getApplicationContext(), R.raw.de_dust); 
			player.setLooping(true); // Set looping 
			player.setVolume(100,100); 
			player.setOnCompletionListener(new OnCompletionListener() {
	            public void onCompletion(MediaPlayer mMediaPlayer) {    
	                mMediaPlayer.release();
	            }
	        });
			player.start(); 

			return null;
		}
	}



	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		mp.release();
		
	}



}
