package com.example.icopter;

import java.util.HashMap;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
// other imports
// ...
public class Sounds{

	public static final int S1 = R.raw.de_dust;

	private static SoundPool soundPool;
	private static HashMap soundPoolMap;
	
	
	public Sounds(){
		initSounds(Variables.mContext);
	}
	
	/** Populate the SoundPool*/
	@SuppressWarnings("unchecked")
	public static void initSounds(Context context) {
		soundPool = new SoundPool(2, AudioManager.STREAM_MUSIC, 100);
		soundPoolMap = new HashMap(3);     
		soundPoolMap.put( S1, soundPool.load(context, S1, 1) );
	}


	/** Play a given sound in the soundPool */
	public static void playSound(Context context, int soundID) {
		if(soundPool == null || soundPoolMap == null){
			initSounds(context);
		}
		float volume = (float) 0.5;// whatever in the range = 0.0 to 1.0
		// play sound with same right and left volume, with a priority of 1, 
		// zero repeats (i.e play once), and a playback rate of 1f
		soundPool.play((Integer) soundPoolMap.get(soundID), volume, volume, 1, 0, 1f);
	}








}