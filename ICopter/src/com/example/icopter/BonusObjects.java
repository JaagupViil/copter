package com.example.icopter;

import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.CountDownTimer;
import android.util.Log;

public class BonusObjects {
	
	public String TAG = "BonusObjects";
	private Random generator = new Random();
	public static Rect paraRectangle;
	public static Bitmap paraTrooper;
	public static Rect eagleRectangle;
	public static Bitmap eagle;
	public static Bitmap fighter;
	public int[] bonusLocations = {0,200,250,300,350};
	Paint p = new Paint();
	public boolean movingTrooper=false;
	public boolean movingEagle=false;
	public boolean jetForm = false;


	public BonusObjects(){
		paraRectangle = new Rect();
		eagleRectangle = new Rect();
		paraTrooper = 
				BitmapFactory.decodeResource(Variables.mContext.getResources(), R.drawable.trooper);
		eagle = 
				BitmapFactory.decodeResource(Variables.mContext.getResources(), R.drawable.eagle);
		fighter = 
				BitmapFactory.decodeResource(Variables.mContext.getResources(), R.drawable.fighter);

		moveEagleToNewCoordinates();
		moveTrooperToNewCoordinates();

	}

	public void moveEagleToNewCoordinates(){
		int moveBonus = bonusLocations[generator.nextInt(4)];
		if(eagleRectangle.top+moveBonus>=0 && eagleRectangle.bottom+moveBonus<=Variables.screenHeight){
			eagleRectangle.set(Variables.screenWidth+25, 300+moveBonus, 
					Variables.screenWidth+200, 400+moveBonus);
		}
		else{
			eagleRectangle.set(Variables.screenWidth+25, 300-moveBonus, 
					Variables.screenWidth+200, 400-moveBonus);
		}
		
	}
	

	public void moveTrooperToNewCoordinates(){
		int moveBonus = bonusLocations[generator.nextInt(4)];
		if(paraRectangle.top+moveBonus>=0 && paraRectangle.bottom+moveBonus<=Variables.screenHeight){
			paraRectangle.set(Variables.screenWidth+100,
					300+moveBonus, Variables.screenWidth+200, 480+moveBonus);
		}
		else{
			paraRectangle.set(Variables.screenWidth+100,
					300-moveBonus, Variables.screenWidth+200, 480-moveBonus);
		}
	}

	public boolean createBonus(int chance){
		if(generator.nextInt(100)>=chance){
			return true;
		}
		return false;
	}

	public void drawBonuses(Canvas canvas){
		//		canvas.drawRect(paraRectangle, p);
		canvas.drawBitmap(paraTrooper, null, paraRectangle, p);
		canvas.drawBitmap(eagle, null, eagleRectangle, p);
		if(movingTrooper){
			moveTrooper();
		}
		if(movingEagle){
			moveEagle();
		}

		detectTrooperCollision(Copter.lowerCopter,Copter.upperCopter);
		detectEagleCollision(Copter.lowerCopter,Copter.upperCopter);
				
	}
	public void moveTrooper(){
		if(paraRectangle.left<=0){
			moveTrooperToNewCoordinates();
			movingTrooper=false;
		}
		paraRectangle.left+=-18;
		paraRectangle.right+=-18;
		paraRectangle.bottom+=1;
		paraRectangle.top+=1;

	}
	public void moveEagle(){
		if(eagleRectangle.left<=0){
			moveEagleToNewCoordinates();
			movingEagle=false;
		}
		eagleRectangle.left+=-25;
		eagleRectangle.right+=-25;
	}

	public void detectTrooperCollision(Rect lowerCopter, Rect upperCopter){
		if(Rect.intersects(paraRectangle, lowerCopter) || Rect.intersects(paraRectangle, upperCopter)){
			moveTrooperToNewCoordinates();
			Blocks.moveBlockHeights=false;
			movingTrooper=false;
			Variables.score+=1000;
			new CountDownTimer(5000, 1000) {

				public void onTick(long millisUntilFinished) {
				}

				public void onFinish() {
					Blocks.moveBlockHeights=true;
				}  
			}.start();
		}

	}

	public void detectEagleCollision(Rect lowerCopter, Rect upperCopter){
		if(Rect.intersects(eagleRectangle, lowerCopter) || Rect.intersects(eagleRectangle, upperCopter)){
			moveEagleToNewCoordinates();
			Variables.copter.setBitmap(fighter);
			Variables.blocks.blocks_speed=20;
			Variables.copter.copter_speed=12;
			Variables.copter.smoke_speed=20-5;
			Variables.score+=5000;
			jetForm = true;
			movingEagle=false;
			new CountDownTimer(10000, 1000) {

				public void onTick(long millisUntilFinished) {
					
				}

				public void onFinish() {
					Variables.blocks.blocks_speed=10;
					Variables.copter.copter_speed=8;
					Variables.copter.setCopterToBitmap();
//					Variables.changeBitmapColor(Color.TRANSPARENT, Variables.copter.smoke);
//					Variables.copter.smoke = Variables.makeTransparent(Variables.copter.smoke, 70);
					movingEagle=false;
					jetForm = false;
					
				}  
			}.start();
		}

	}

}
