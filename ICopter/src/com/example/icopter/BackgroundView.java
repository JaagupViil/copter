package com.example.icopter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class BackgroundView extends View {
	

	private final static int NUM_PARTICLES = 25;
	private final static int FRAME_RATE = 30;
	private final static int LIFETIME = 300;
	private static boolean play_game = true;
	private Handler mHandler;
	public String TAG = "BackgroundView";
	Paint p = new Paint();
	public BackgroundView(Context context, AttributeSet attrs) {
		
		super(context, attrs);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {

	}

	@Override
	protected void onDraw(Canvas canvas) {
		Variables.copter.drawCopter(canvas);
		p.setStyle(Paint.Style.STROKE);
		p.setColor(Color.BLACK);
		p.setStrokeWidth(100);
		Variables.blocks.drawBlocks(canvas);
		canvas.drawRect(0, 0, Variables.screenWidth, Variables.screenHeight, p);   
		canvas.drawRect(100, Variables.screenHeight-100, Variables.screenWidth-100, Variables.screenHeight, p);   
		detectCollision();
		Variables.bonus.drawBonuses(canvas);
		p.setColor(Color.WHITE);
		p.setStyle(Paint.Style.FILL);
		p.setTextSize(72);
		canvas.drawText("Score: "+Variables.score/10, 100, Variables.screenHeight-50, p);
		canvas.drawText("Best: "+Variables.highscore, Variables.screenWidth-500, Variables.screenHeight-50, p);
		invalidate();
	}


	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction()==MotionEvent.ACTION_DOWN){
			Variables.copter.touchScreen=true;
		}
		else if(event.getAction()==MotionEvent.ACTION_UP){
			Variables.copter.touchScreen=false;
		}
		return true;
	}
	
	public void detectCollision(){
		if(Variables.blocks.detectCollison(Variables.copter.upperCopter) || Variables.blocks.detectCollison(Variables.copter.lowerCopter)){
			play_game = false;
			stopGame();
			AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
			builder.setCancelable(false);
			builder.setMessage("Do you want to play again?");
			builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					// FIRE ZE MISSILES!
					startGame();
				}
			});
			builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					System.exit(1);
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}
	
	public void updateScore(){
		int oldScore = Variables.mPrefs.getInt("Highscore", 0);
		int currentScore = (int) (Variables.score/10);
		if(oldScore < currentScore){
			Variables.editor.putInt("Highscore", currentScore);
			Variables.editor.commit();
			Variables.highscore = currentScore;
			Log.i("Backgroundview", oldScore + " " + currentScore);
		}
	}
	
//	a lot of variables to reset
	public void stopGame(){
		updateScore();
		int left = Variables.screenWidth-Variables.blocks.blockWidth+25;
    	int right = Variables.screenWidth+25;
		Variables.blocks.obstacle1.set(left, (Variables.screenHeight/2)-100, right, (Variables.screenHeight/2)+100);
    	Variables.blocks.obstacle2.set(left+right/2, (Variables.screenHeight/2)-100, right+right/2, (Variables.screenHeight/2)+100);
		int centerY = Variables.screenHeight/2;
		Variables.blocks.moveBlockHeights=true;
		Variables.copter.setCopterToBitmap();
		Variables.bonus.jetForm=false;
		Variables.bonus.movingEagle=false;
		Variables.bonus.movingTrooper=false;
		Variables.bonus.moveEagleToNewCoordinates();
		Variables.bonus.moveTrooperToNewCoordinates();
		Variables.copter.copterY = centerY;
		Variables.copter.copterX = centerY;
		Variables.copter.copter_speed=0;
		Variables.blocks.blocks_speed=0;
		Variables.blocks.score_addition=0;
		Variables.score=0;
		Variables.copter.smoke_speed=0;
	}
	
	public void startGame(){
		Variables.copter.copter_speed=8;
		Variables.blocks.blocks_speed=10;
		Variables.blocks.score_addition=1;
		Variables.copter.smoke_speed=Variables.blocks.blocks_speed-5;
	}
}
